Gem::Specification.new do |s|
  s.name        = 'home_finance'
  s.version     = '0.0.0'
  s.summary     = "Home finance"
  s.description = "A ruby interface for home.finance.ua API"
  s.authors     = ["andriy"]
  s.files       = ["lib/home_finance.rb"]
  s.homepage    = 'http://rubygems.org/gems/home_finance'
end
