require 'byebug'

require 'net/http'
require 'json'
require 'date'

require_relative 'home_finance/deps_manager'
require_relative 'home_finance/service_factory'
require_relative 'home_finance/session'
require_relative 'home_finance/client'
require_relative 'home_finance/sender'
require_relative 'home_finance/compounder'
require_relative 'home_finance/input_normalizer'

require_relative 'home_finance/services'
require_relative 'home_finance/services/settings'
require_relative 'home_finance/services/auth'
require_relative 'home_finance/services/operation'

require_relative 'home_finance/fields'
require_relative 'home_finance/fields/settings_read_fields'
require_relative 'home_finance/fields/settings_write_fields'
require_relative 'home_finance/fields/auth_auth_fields'
require_relative 'home_finance/fields/operation_read_fields'
require_relative 'home_finance/fields/operation_write_fields'

require_relative 'home_finance/structure_array'
require_relative 'home_finance/structure_hash'
require_relative 'home_finance/structure_elements'
require_relative 'home_finance/structures'
require_relative 'home_finance/structures/auth_auth_structure'
require_relative 'home_finance/structures/operation_read_structure'
require_relative 'home_finance/structures/operation_write_structure'
require_relative 'home_finance/structures/settings_read_structure'
require_relative 'home_finance/structures/settings_write_structure'

module HomeFinance

  LOCATION = 'http://home.finance.ua/services/index.php'
  SERVICES = [:auth, :settings, :operation]

end

# client = HomeFinance::Client.new
# client.send_request(service: :auth, content: {email: 'example@gmail.com', password: 'password'})
# client.send_request(service: :settings, action: :write, content: {asd: "ctgs", zxc: "currencies", qwe: "accountDate"})
# client.send_request(service: :settings, action: :read, content: ["ctgs", "currencies", "accountDate"])
# client.send_request(service: :operation, action: :delete, content: [{date: '02.05.2015', sum: '200', comment: 'test'}, {sum: '500', comment: 'test_2'}])
# client.send_request(service: :operation, action: :read, content: {fromdate: '02.02.2016', todate: '02.03.2016', today: '21.03.2016'})