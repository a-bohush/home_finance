module HomeFinance
  class Client

    attr_accessor :session, :service_factory, :deps_manager, :input_normalizer

    def initialize(session: Session, service_factory: ServiceFactory, deps_manager: DepsManager.new)
      @session = session
      @service_factory = service_factory
      @deps_manager = deps_manager
      @input_normalizer = deps_manager.input_normalizer
    end

    def logout
      session.unset_credentials
    end

    def send_request(service:, action: nil, content: nil)
      input = input_normalizer.new({service: service, action: action}).input
      service(input[:service]).perform(input[:action], content)
    end

    def credentials
      session.credentials
    end

    private

    def service(requested_service)
      service_factory.provide_service(requested_service, deps_manager)
    end
  end
end
