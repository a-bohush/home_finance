module HomeFinance
  class AuthAuthFields < Fields::Base

    def params_email
      content[:email]
    end

    def params_password
      content[:password]
    end

  end
end
