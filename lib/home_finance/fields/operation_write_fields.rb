module HomeFinance
  class OperationWriteFields < Fields::Base

    def params_operationActionType
      requester.actions.index(requester.action)
    end

    def params_operationObject_id
      id(false)
    end

    def params_operationObject_type
      content[yield][:type] || 0
    end

    def params_operationObject_date
      date_to_timestamp(content[yield][:date])
    end

    def params_operationObject_comment
      content[yield][:comment] || 0
    end

    def params_operationObject_sum
      content[yield][:sum] || 0
    end

    def params_operationObject_idCurrency
      content[yield][:idCurrency] || 0
    end

    def params_operationObject_idCtg
      content[yield][:idCtg] || 0
    end

    def params_operationObject_idSchet
      content[yield][:idSchet] || 0
    end

  end
end
