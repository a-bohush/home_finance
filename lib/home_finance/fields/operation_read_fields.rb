module HomeFinance
  class OperationReadFields < Fields::Base

    def content
      super[0]
    end

    def params_fromdate
      date_to_timestamp(content[:fromdate])
    end

    def params_todate
      date_to_timestamp(content[:todate])
    end

    def params_today
      date_to_timestamp(content[:today])
    end

  end
end
