module HomeFinance
  class StructureHash < Hash

    def value_by_index(index)
      self[keys[index]]
    end

    def key_by_index(index)
      keys[index]
    end

  end
end
