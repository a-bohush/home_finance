module HomeFinance
  class StructureArray < Array

    def value_by_index(index)
      self[index]
    end

    def map_with_index
      mapped = []
      for el in self
        block_value = yield el, self.index(el)
        mapped << block_value unless block_value.nil?
      end
      mapped
    end

  end
end
