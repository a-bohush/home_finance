module HomeFinance
  class OperationWriteStructure < Structures::Base

    def structure
      structure = request_header
      structure[:params] = structure_array
      structure[:params] << credentials
      structure[:params] << add_operations
      structure
    end

    private

    def add_operations
      export = structure_array
      content.each do
        write_operation = self.write_operation
        write_operation[:operationObject] = write_operation_object
        export << write_operation
      end
      export
    end

  end
end
