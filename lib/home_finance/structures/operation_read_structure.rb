module HomeFinance
  class OperationReadStructure < Structures::Base

    def structure
      structure = request_header
      structure[:params] = structure_array
      structure[:params] << credentials
      structure[:params] << read_operation_set
      structure
    end

  end
end
