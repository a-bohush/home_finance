module HomeFinance
  class SettingsWriteStructure < Structures::Base

    def structure
      structure = request_header
      structure[:params] = structure_array
      structure[:params] << credentials
      structure[:params] << structure_hash
      structure
    end

  end
end
