module HomeFinance
  class AuthAuthStructure < Structures::Base

    def structure
      structure = request_header
      structure[:params] = structure_array
      structure[:params] << authorization_set
      structure
    end

  end
end