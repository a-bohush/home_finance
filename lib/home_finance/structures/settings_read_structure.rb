module HomeFinance
  class SettingsReadStructure < Structures::Base

    def structure
      structure = request_header
      structure[:params] = structure_array
      structure[:params] << credentials
      structure[:params] << structure_array
      structure
    end

  end
end
