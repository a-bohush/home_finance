module HomeFinance
  class Session

    def self.set_credentials(credentials)
      @credentials = parse(credentials)
    end

    def self.credentials
      @credentials
    end

    def self.unset_credentials
      @credentials = {}
    end

    def self.token
      credentials['token']
    end

    def self.id_user
      credentials['idUser']
    end

    def self.id_account
      credentials['idAccount']
    end

    def self.role
      credentials['role']
    end

    private
    def self.parse(credentials)
      JSON.parse(credentials)['result']
    end

  end
end
