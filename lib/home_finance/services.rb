module HomeFinance::Services
  class Base

    attr_accessor :sender, :compounder, :session, :fields, :structure

    def initialize(deps)
      setup_service
      @sender = deps.sender
      @compounder = deps.compounder
      @session = deps.session
      @fields = deps.fields
      @structure = deps.structure
    end

    def perform(action=nil, content=nil)
      self.action = action if action
      self.content = content if content
      sender.request(request_data)
    end

    def fetch(info)
      public_methods.include?(info) ? send(info) : ''
    end

    private
    def request_data
      compounder.new(fields.container(self)).fill_structure(structure.structure(self))
    end

  end
end
