module HomeFinance
  class ServiceFactory
    @services = {}

    class << self
      attr_accessor :services
    end

    def self.provide_service(service, deps_manager)
      services[service] ||= Object.const_get("HomeFinance::#{service.to_s.capitalize}").new(deps_manager)
    end

  end
end
