module HomeFinance
  class Compounder

    attr_accessor :fields_container

    def initialize(fields_container)
      @fields_container = fields_container
    end

    def fill_structure(structure, prefix='', i='')
      return fields_container.send("#{prefix}#{i}") if structure.empty?

      if structure.is_a?(StructureArray)
        return structure.map_with_index { |structure, i| fill_structure(structure, "#{prefix}", i) }
      end

      index = 0
      begin
        structure[structure.key_by_index(index)] = process_current_value(value: structure.value_by_index(index),
                                                                         index: index,
                                                                         structure: structure,
                                                                         prefix: prefix,
                                                                         i: i)
        index += 1
      end while structure.size > index
      structure
    end

    private
    def process_current_value(options)
      return process_value_nil(options) if options[:value].nil?
      return process_value_hash(options) if options[:value].is_a? StructureHash
      return process_value_array(options) if options[:value].is_a? StructureArray
    end

    def process_value_nil(options)
      fields_container.send("#{options[:prefix]}#{options[:structure].key_by_index(options[:index])}") { options[:i] }
    end

    def process_value_hash(options)
      send_key = options[:prefix].dup << options[:structure].key_by_index(options[:index]).to_s
      fill_structure(options[:structure].value_by_index(options[:index]), "#{send_key}_", options[:i])
    end

    def process_value_array(options)
        send_key = options[:prefix].dup << options[:structure].key_by_index(options[:index]).to_s
        options[:value].map_with_index { |structure, i| fill_structure(structure, "#{send_key}_", i) }
    end

  end
end
