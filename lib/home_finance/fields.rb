module HomeFinance::Fields
  class Base
    attr_reader :session, :requester, :service, :method, :content

     def initialize(requester)
      @requester = requester
      @session = requester.fetch(:session)
      @service = requester.fetch(:service)
      @method = requester.fetch(:method)
      @content = requester.fetch(:content)
    end

    def self.container(requester)
      descendant = "#{requester.class}#{requester.fetch(:method).capitalize}Fields"
      Object.const_get(descendant).new(requester)
    end

    def id(value = true)
      if value
        ENV['FINANCE_TEST'] ? 1 : (Random.new.rand * 100).round
      else
        ENV['FINANCE_TEST'] ? -1 : -(Random.new.rand * 100).round
      end
    end

    def params_token
      session.token
    end

    def params_idUser
      session.id_user
    end

    def params_idAccount
      session.id_account
    end

    def params_role
      session.role
    end

    protected
    def date_to_timestamp(date=nil)
      date ? DateTime.parse(date).to_time.to_i : DateTime.parse(Date.today.strftime).to_time.to_i
    end

  end
end
