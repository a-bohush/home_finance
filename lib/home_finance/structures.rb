module HomeFinance::Structures
  class Base

    include HomeFinance::StructureElements

    attr_reader :method, :action, :content

    def initialize(requester)
      @method = requester.fetch(:method)
      @action = requester.fetch(:action)
      @content = requester.fetch(:content)
    end

    def self.structure(requester)
      descendant = "#{requester.class}#{requester.fetch(:method).capitalize}Structure"
      Object.const_get(descendant).new(requester).structure
    end

  end
end
