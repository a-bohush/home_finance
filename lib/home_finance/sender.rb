module HomeFinance
  class Sender

    attr_accessor :uri, :http, :post

    def initialize(uri: URI, http: Net::HTTP, post: Net::HTTP::Post)
      @uri = uri.parse(LOCATION)
      @http = http.new(self.uri.host, self.uri.port)
      @post = post.new(self.uri.request_uri)
      self.post["Content-Type"] = "application/json"
    end

    def request(data)
      post.body = data
      http.request(post)

      # byebug  #data
      # "{\"result\":{\"token\":\"ff6ab8de825af1c1e887610d2a8ec71f\",\"idUser\":54526,\"idAccount\":54526,\"role\":1},\"id\":54}"
    end

  end
end
