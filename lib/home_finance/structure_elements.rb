module HomeFinance
  module StructureElements

    def authorization_set
      StructureHash[
        email: nil,
        password: nil
      ]
    end

    def credentials
      StructureHash[
        token: nil,
        idUser: nil,
        idAccount: nil,
        role: nil
      ]
    end

    def request_header
      StructureHash[
        id: nil,
        service: nil,
        method: nil,
        params: nil
      ]
    end

    def write_operation
      StructureHash[
        operationActionType: nil,
        operationObject: nil
      ]
    end

    def write_operation_object
      StructureHash[
        id: nil,
        type: nil,
        date: nil,
        comment: nil,
        sum: nil,
        idCurrency: nil,
        idCtg: nil,
        idSchet: nil
      ]
    end

    def structure_array
      StructureArray[]
    end

    def structure_hash
      StructureHash[]
    end

    def read_operation_set
      StructureHash[
        fromdate: nil,
        todate: nil,
        today: nil
      ]
    end

  end
end
