module HomeFinance
  class Operation < Services::Base

    attr_accessor :actions, :methods, :content, :service, :action

    def setup_service
      @service = 'soperations'
      @methods = ['write', 'read']
      @actions = ['new', 'edit', 'delete']
      @action = nil
      @content = nil
    end

    def method
      actions.include?(action) ? 'write' : 'read'
    end

    def action
      @action.to_s
    end

    def content
      @content.is_a?(Array) ? @content : [@content]
    end

  end
end
