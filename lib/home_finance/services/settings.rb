module HomeFinance
  class Settings < Services::Base

    attr_accessor :service, :content, :action

    def setup_service
      @service = 'settings'
      @methods = ['read', 'write']
      @action = nil
      @content = nil
    end

    def method
      action
    end

    def action
      @action.to_s
    end

  end
end
