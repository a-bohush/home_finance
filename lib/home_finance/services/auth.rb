module HomeFinance
  class Auth < Services::Base

    attr_accessor :service, :content

    def setup_service
      @service = 'auth'
      @method = method
      @content = nil
    end

    def perform(action, content)
      session.set_credentials(super)
    end

    def method
      service.to_s
    end

  end
end
