module HomeFinance
  class DepsManager

    attr_accessor :sender, :compounder, :session, :fields, :structure, :input_normalizer

    def initialize(sender: Sender.new,
                   compounder: Compounder,
                   session: Session,
                   fields: Fields::Base,
                   structure: Structures::Base,
                   input_normalizer: InputNormalizer)
      @sender = sender
      @compounder = compounder
      @session = session
      @fields = fields
      @structure = structure
      @input_normalizer = input_normalizer
    end

  end
end
