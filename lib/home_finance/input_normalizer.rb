module HomeFinance
  class InputNormalizer

    attr_accessor :input

    def initialize(input)
      @input = input
      normalize_service
      normalize_action
    end

    private
    def normalize_action
      input[:action] = input[:action].to_s if input[:action]
    end

    def normalize_service
      if SERVICES.include?(input[:service].to_sym)
        input[:service] = input[:service].to_sym
      else
        raise 'Unknown service'
      end
    end

  end
end