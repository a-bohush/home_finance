require 'minitest/autorun'
require 'minitest/mock'
require_relative '../lib/home_finance'

describe HomeFinance::Client do

  let(:request) {
    {service: :operation, action: :delete, content: 'content'}
  }

  before do
    @input_normalizer = Minitest::Mock.new.expect(:input, {service: request[:service].to_sym, action: request[:action].to_s})
    @input_normalizer_class = Minitest::Mock.new.expect(:new, @input_normalizer, [{service: request[:service], action: request[:action]}])
    @service = Minitest::Mock.new.expect(:perform, 'responce', [request[:action].to_s, request[:content]])
    @session = Minitest::Mock.new
    @deps_manager = Minitest::Mock.new.expect(:input_normalizer, @input_normalizer_class)
    @service_factory = Minitest::Mock.new.expect(:provide_service, @service, [request[:service].to_sym, @deps_manager])
    @client = HomeFinance::Client.new(session: @session, service_factory: @service_factory, deps_manager: @deps_manager)
  end

  describe '#credentials' do
    it 'gets credentials from session' do
      @session.expect(:credentials, 'some credentials')

      @client.credentials

      @session.verify
    end
  end

  describe '#logout' do
    it 'calls unset credentials in session' do
      @session.expect(:unset_credentials, {})

      @client.logout

      @session.verify
    end
  end

  describe '#send_request' do
    it 'calls perform onto object returned by service with action and content as arguments' do
      @client.send_request(request)

      @service.verify
    end

    it 'calls input onto input_normalizer instance' do
      @client.send_request(request)

      @input_normalizer.verify
    end

    it 'calls new onto input_normalizer' do
      @client.send_request(request)

      @input_normalizer_class.verify
    end

    it 'returnes string of responce' do
      assert_kind_of String, @client.send_request(request)
    end

  end

  describe '#new' do
    it 'sets session value to @session' do
      assert_same @client.session.object_id, @session.object_id
    end

    it 'sets service_factory value to @service_factory' do
      assert_same @client.service_factory.object_id, @service_factory.object_id
    end

    it 'sets deps_manager value to @deps_manager' do
      assert_same @client.deps_manager.object_id, @deps_manager.object_id
    end

    it 'sets deps_manager.input_normalizer to @input_normalizer' do
      assert_same @client.input_normalizer.object_id, @input_normalizer_class.object_id
    end
  end

end
