require 'minitest/autorun'
require 'minitest/mock'
require 'webmock/minitest'
require_relative '../lib/home_finance'

describe HomeFinance::Sender do

  let(:data) {
    "JSON string"
  }

  before do
    @sender = HomeFinance::Sender.new
    @stub_post = stub_request(:post, HomeFinance::LOCATION).
      with(:body => data,
           :headers => {'Content-Type'=>'application/json'})
  end

  describe '#new' do
    it 'sets uri to @uri' do
      refute_nil(@sender.uri)
    end

    it 'sets http to @http' do
      refute_nil(@sender.http)
    end

    it 'sets post to @post' do
      refute_nil(@sender.post)
    end

    it 'sets post["Content-Type"] to application/json' do
      assert_equal(@sender.post["Content-Type"], "application/json")
    end
  end

  describe '#request' do
    it 'sets post.body to data' do
      @sender.request(data)

      assert_same(@sender.post.body, data)

      WebMock.reset!
    end

    it 'calls http.request with post as argument' do
      @sender.request(data)

      assert_requested(@stub_post)

      WebMock.reset!
    end
  end

end
