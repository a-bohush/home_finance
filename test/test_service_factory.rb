require 'minitest/autorun'
require 'minitest/mock'
require_relative '../lib/home_finance'

describe HomeFinance::ServiceFactory do

  before do
    @service_factory = HomeFinance::ServiceFactory
    @service_factory.services = {}
    @service = :mocked
    @service_instance = Minitest::Mock.new
    @deps_manager = Minitest::Mock.new
  end

  describe '.provide_service' do

    it 'returnes new instance of requested service if it isn\'t present in @services' do
      HomeFinance::Mocked = Minitest::Mock.new.expect(:new, @service_instance, [@deps_manager])

      assert_same @service_factory.provide_service(@service, @deps_manager).object_id, @service_instance.object_id

      HomeFinance.send(:remove_const, :Mocked)
    end

    it 'returnes instance of requested service from @services if there is one' do
      @service_factory.services = {@service => @service_instance}

      assert_same @service_factory.provide_service(@service, @deps_manager).object_id, @service_instance.object_id
    end

    it 'sets created service into services' do
      HomeFinance::Mocked = Minitest::Mock.new.expect(:new, @service_instance, [@deps_manager])

      @service_factory.provide_service(@service, @deps_manager)

      assert_same @service_factory.services[@service].object_id, @service_instance.object_id

      HomeFinance.send(:remove_const, :Mocked)
    end
  end

end
