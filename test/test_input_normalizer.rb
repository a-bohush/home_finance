require 'minitest/autorun'
require 'minitest/mock'
require_relative '../lib/home_finance'

describe HomeFinance::InputNormalizer do

  let(:input) {
    {service: :operation, action: :delete, content: 'content'}
  }

  before do
    @normalizer = HomeFinance::InputNormalizer.new(input)
  end

  describe "#new" do
    it 'sets input to @input' do
      assert_same @normalizer.input, input
    end
  end

  describe '#normalized_input' do
    it 'returnes changed input with service value converted to string if value was present' do
      assert_kind_of Symbol, @normalizer.input[:service]
    end

    it 'returnes changed input with action value converted to string' do
      assert_kind_of String, @normalizer.input[:action]
    end

  end

  it 'raises an exception if given service isn`t present in a SERVICES' do
    input = {service: :unknown_service, action: :delete, content: 'content'}

    assert_raises(RuntimeError) { HomeFinance::InputNormalizer.new(input) }
  end

end
