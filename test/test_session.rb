require 'minitest/autorun'
require 'minitest/mock'
require_relative '../lib/home_finance'

describe HomeFinance::Session do

  let(:responce) {
    "{\"result\":{\"token\":\"ff6ab8de825af1c1e887610d2a8ec71f\",\"idUser\":54526,\"idAccount\":54526,\"role\":1},\"id\":54}"
  }

  let(:parsed_responce) {
    JSON.parse(responce)['result']
  }

  before do
    @session = HomeFinance::Session
    @session.set_credentials(responce)
  end

  describe '.set_credentials' do
    it 'sets hash with credentials parsed from responce to @credentials' do
      assert_equal(@session.credentials, parsed_responce)
    end
  end

  describe '.unset_credentials' do
    it 'clears hash been seted @credentials' do
      assert_empty @session.unset_credentials
    end
  end

  describe '.token' do
    it 'returnes token value from @credentials' do
      assert_equal @session.token, parsed_responce['token']
    end
  end

  describe '.id_user' do
    it 'returnes idUser value from @credentials' do
      assert_equal @session.id_user, parsed_responce['idUser']
    end
  end

  describe '.id_account' do
    it 'returnes idAccount value from @credentials' do
      assert_equal @session.id_account, parsed_responce['idAccount']
    end
  end

  describe '.role' do
    it 'returnes role value from @credentials' do
      assert_equal @session.role, parsed_responce['role']
    end
  end

end
